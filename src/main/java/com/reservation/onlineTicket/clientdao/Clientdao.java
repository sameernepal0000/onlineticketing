package com.reservation.onlineTicket.clientdao;

import com.reservation.onlineTicket.models.LoginDetails;
import com.reservation.onlineTicket.models.ReservationDetails;
import com.reservation.onlineTicket.models.Response;

import java.util.List;

public interface Clientdao {

    Response checkLogindetail(LoginDetails loginDetails);

    Response saveReservationDetails(ReservationDetails details);

    Response savePayment(ReservationDetails details);

    List<ReservationDetails> getReservationHistory();
}
