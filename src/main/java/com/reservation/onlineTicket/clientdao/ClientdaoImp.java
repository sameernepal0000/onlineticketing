package com.reservation.onlineTicket.clientdao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reservation.onlineTicket.entity.LoginCredentials;
import com.reservation.onlineTicket.models.LoginDetails;
import com.reservation.onlineTicket.models.ReservationDetails;
import com.reservation.onlineTicket.models.Response;
import com.reservation.onlineTicket.repository.LoginRepository;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
@Repository
public class ClientdaoImp implements Clientdao {
    static Logger log = LoggerFactory.getLogger(ClientdaoImp.class);
    @Autowired
    LoginRepository repository;
   String jwt;
    @Override
    public Response checkLogindetail(LoginDetails loginDetails) {
        Response responses=new Response();
        try {
            LoginCredentials loginCredentials = repository.findByUsername(loginDetails.getUsername());
            if (loginCredentials.getPasswords().equals(loginDetails.getPassword())) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("username",loginDetails.getUsername());
                jsonObject.put("password",loginCredentials.getPasswords());

                BufferedReader rd = null;
                try {
                    HttpClient httpClient = HttpClientBuilder.create().build();
                    HttpPost post = new HttpPost("http://localhost:8080/authenticate");
                    post.setHeader("Content-type", "application/json");
                    post.setHeader("Accept", "application/json");
                    StringEntity input = new StringEntity(jsonObject.toString());

                    post.setEntity(input);
                    HttpResponse response = httpClient.execute(post);
                    rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                    StringBuilder content = new StringBuilder();
                    String line;
                    while (null != (line = rd.readLine())) {
                        content.append(line);
                    }
                    JSONObject jsonObj1 = new JSONObject(content.toString());
                    jwt = jsonObj1.optString("jwt");
                    responses.setStatus(true);
                    responses.setMassage("Sucessfully Verified");
                    System.out.println(jwt);
                } catch (Exception e) {
                    log.error("Error: " + e.toString());
                } finally {
                    try {
                        rd.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }else{
                responses.setStatus(false);
                responses.setMassage("username and password incorrect");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return responses;
    }



    @Override
    public Response saveReservationDetails(ReservationDetails details) {
        Response responses=new Response();
        try {
               JSONObject jsonObject = new JSONObject();
                jsonObject.put("fullname",details.getFullname());
                jsonObject.put("moviename", details.getMoviename());
                jsonObject.put("circle",details.getCircle());
                jsonObject.put("phone", details.getPhone());
                jsonObject.put("tickets",details.getTickets());
                jsonObject.put("childrens", details.getChildrens());
                jsonObject.put("bookeddate", details.getBookeddate());
                jsonObject.put("showdate",details.getShowdate());
                jsonObject.put("totalamount", details.getTotalamount());
                jsonObject.put("uniqueNumber", details.getUniqueNumber());
                BufferedReader rd = null;
                try {
                    HttpClient httpClient = HttpClientBuilder.create().build();
                    HttpPost post = new HttpPost("http://localhost:8080/saveReservationdetaion");
                    post.setHeader("Content-type", "application/json");
                    post.setHeader("Accept", "application/json");
                    post.setHeader("Authorization","Bearer "+jwt);
                    StringEntity input = new StringEntity(jsonObject.toString());

                    post.setEntity(input);
                    HttpResponse response = httpClient.execute(post);
                    rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                    StringBuilder content = new StringBuilder();
                    String line;
                    while (null != (line = rd.readLine())) {
                        content.append(line);
                    }
                    JSONObject jsonObj1 = new JSONObject(content.toString());
                   String responsemassage = jsonObj1.optString("massage");
                   boolean statud=jsonObj1.optBoolean("status");
                   responses.setStatus(statud);
                   responses.setMassage(responsemassage);
                    System.out.println(responsemassage);
                    System.out.println(statud);
                } catch (Exception e) {
                    log.error("Error: " + e.toString());
                } finally {
                    try {
                        rd.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

        }
        catch (Exception e){
            e.printStackTrace();
        }

        return responses;
    }

    @Override
    public Response savePayment(ReservationDetails details) {
        Response responses=new Response();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fullname",details.getFullname());
            jsonObject.put("cvcNo", details.getCvcNo());
            jsonObject.put("year",details.getYear());
            jsonObject.put("months", details.getMonths());
            jsonObject.put("cardNo",details.getCardNo());
            jsonObject.put("uniqueNumber", details.getUniqueNumber());
            BufferedReader rd = null;
            try {
                HttpClient httpClient = HttpClientBuilder.create().build();
                HttpPost post = new HttpPost("http://localhost:8080/savePayment");
                post.setHeader("Content-type", "application/json");
                post.setHeader("Accept", "application/json");
                post.setHeader("Authorization","Bearer "+jwt);
                StringEntity input = new StringEntity(jsonObject.toString());

                post.setEntity(input);
                HttpResponse response = httpClient.execute(post);
                rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuilder content = new StringBuilder();
                String line;
                while (null != (line = rd.readLine())) {
                    content.append(line);
                }
                JSONObject jsonObj1 = new JSONObject(content.toString());
                String responsemassage = jsonObj1.optString("massage");
                boolean statud=jsonObj1.optBoolean("status");
                responses.setStatus(statud);
                responses.setMassage(responsemassage);
                System.out.println(responsemassage);
                System.out.println(statud);
            } catch (Exception e) {
                log.error("Error: " + e.toString());
            } finally {
                try {
                    rd.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

        return responses;
    }


    @Override
    public List<ReservationDetails> getReservationHistory() {
        List<ReservationDetails> uu = new ArrayList<>();
        BufferedReader rd = null;

        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet postRequest = new HttpGet("http://localhost:8080/getReservationHistory");

            postRequest.setHeader("Content-type", "application/json");
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Authorization","Bearer "+jwt);

            HttpResponse response = httpClient.execute(postRequest);
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));


            StringBuilder content = new StringBuilder();
            String line;
            while (null != (line = rd.readLine())) {
                content.append(line);
            }
            Gson gson = new Gson();
            Type listType = new TypeToken<List<ReservationDetails>>() {
            }.getType();
            uu = gson.fromJson(content.toString(), listType);
            System.out.println(uu);

        } catch (Exception e) {

            log.error(e.toString());
            throw new RuntimeException(e);
        } finally {
            try {
                rd.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return uu;
    }
}
