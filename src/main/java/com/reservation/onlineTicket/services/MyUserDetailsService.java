package com.reservation.onlineTicket.services;

import com.reservation.onlineTicket.dao.OnlineTicketDao;
import com.reservation.onlineTicket.entity.LoginCredentials;
import com.reservation.onlineTicket.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class MyUserDetailsService implements UserDetailsService {
     @Autowired
     OnlineTicketDao onlineTicketDao;

     @Autowired
     LoginRepository loginRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        LoginCredentials loginCredentials = loginRepository.findByUsername(userName);

        return new User(loginCredentials.getUserName(),loginCredentials.getPasswords(),new ArrayList<>());
    }
}
