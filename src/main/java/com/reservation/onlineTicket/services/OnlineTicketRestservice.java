package com.reservation.onlineTicket.services;

import com.reservation.onlineTicket.entity.LoginCredentials;
import com.reservation.onlineTicket.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnlineTicketRestservice {
    @Autowired
    LoginRepository loginRepository;

    public void startDataBase(LoginCredentials loginCredentials) {

        loginRepository.save(loginCredentials);
    }
}
