package com.reservation.onlineTicket.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateTimeUtil {
    public static java.sql.Date getCotivitiDate(String s) {
        java.sql.Date sql = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date parsed = new Date();
            parsed = format.parse(s);
             sql = new java.sql.Date(parsed.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sql;
    }
    public static java.sql.Time getCotivitTime(String s) {
        java.sql.Time sql = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            Date parsed = new Date();
            parsed = new Date();
            parsed = format.parse(s);
            sql = new java.sql.Time(parsed.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sql;
    }
    public static Timestamp getCotivitiTimeStamp(String s) {
        Timestamp sql = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date parsed = format.parse(s);
            sql = new Timestamp(parsed.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sql;
    }

    public static Date lastDateOfMonth(){
        Date today = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);

        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);

        Date lastDayOfMonth = calendar.getTime();

        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("Today            : " + sdf.format(today));
        System.out.println("Last Day of Month: " + sdf.format(lastDayOfMonth));

        return lastDayOfMonth;
    }
    public static java.sql.Date getSQLDate(String s) {
        java.sql.Date sql = null;
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date parsed;
            parsed = format.parse(s);
            sql = new java.sql.Date(parsed.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sql;
    }

    public static java.sql.Time getSQLTime(String s) {
        java.sql.Time sql = null;
        try {
            DateFormat format = new SimpleDateFormat("HH:mm");
            Date parsed = format.parse(s);
            DateFormat amPmFormat = new SimpleDateFormat("hh:mm a");
            Date formatted = amPmFormat.parse(amPmFormat.format(parsed));
            sql = new java.sql.Time(formatted.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sql;
    }

    public static Timestamp getSQLTimeStamp(String s) {
        Timestamp sql = null;
        try {
            DateFormat format = new SimpleDateFormat("hh:mm:ss a");
            Date parsed;
            parsed = format.parse(s);
            sql = new Timestamp(parsed.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sql;
    }

    public static long getTimeDifference(Timestamp standardTime, Timestamp customTime) {
        long difference = 0;    // milliseconds
        try {
            long standardTimeInMilliSec = standardTime.getTime();
            long customTimeInMilliSec = customTime.getTime();
            difference = customTimeInMilliSec - standardTimeInMilliSec;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return difference;
    }

    public static long getDateDifference(String startDate, String endDate) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date start = format.parse(startDate);
        Date end = format.parse(endDate);
        long difference = Math.abs(start.getTime() - end.getTime());

        return TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
    }

    public static String getEnglishMonth(String date) {
        DateFormat stringMonthFormat = new SimpleDateFormat("MMMM");
        String month = null;
        try {
            DateFormat standard = new SimpleDateFormat("yyyy-MM-dd");
            Date parsed = standard.parse(date);
            month = stringMonthFormat.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return month;
    }

    public static int getLateDays(String startDate, String endDate) {
        int count = 0;
        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);
        LocalDate temp = start;
        while (temp.isBefore(end)) {
            String day = temp.getDayOfWeek().name();
            System.out.println(day);
            temp = temp.plusDays(1);
            if (day.toLowerCase().equals("saturday")) {
                continue;
            }
            count++;
        }

        return count;
    }

    public static boolean isInRange(String startDate, String endDate, String customDate) throws ParseException {
        boolean flag = false;
        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(customDate);
        LocalDate custom = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        while (start.isBefore(end)) {
            if (custom.equals(start)) {
                flag = true;
                break;
            }
            start = start.plusDays(1);
        }

        return flag;
    }
}
