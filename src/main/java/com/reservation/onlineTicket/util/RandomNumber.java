package com.reservation.onlineTicket.util;

import java.util.Random;

public class RandomNumber {
    public static String getRandomNumericNumber(int num_digit) {
        Random rand = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < num_digit; i++) {

            int rand_num = rand.nextInt(10);
            if (i == 0 && rand_num == 0) {
                rand_num = rand.nextInt(10);
            }
            sb.append(rand_num);

        }
        String result = sb.toString();
        return result;

    }

    public static String getRandomAlphaNumeric(int num_digit) {
        Random rand = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < num_digit; i++) {
            int rand_num = rand.nextInt(51);
            //26 upper char, 9 digit, 26 lower char
            char alphaNum;
            if (rand_num < 26) {
                alphaNum = (char) (rand_num + 'A');
            } else {
                if (rand_num < 35) {
                    alphaNum = (char) (rand_num - 26 + '0');
                } else {
                    alphaNum = (char) (rand_num - 35 + 'a');
                }
            }

            sb.append(alphaNum);

        }
        String result = sb.toString();
        return result;

    }
}