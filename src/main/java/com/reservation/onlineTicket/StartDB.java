package com.reservation.onlineTicket;

import com.reservation.onlineTicket.dao.OnlineTicketDao;
import com.reservation.onlineTicket.services.OnlineTicketRestservice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
@Component
public class StartDB {

    @Autowired
    OnlineTicketRestservice onlineTicketRestDao;
    @Autowired
    OnlineTicketDao onlineTicketDao;

    @PostConstruct
    public void startDataBase() {
        Logger log = LoggerFactory.getLogger(StartDB.class);
        log.debug("JAZZ initialization");
      boolean flag= onlineTicketDao.startDataBase();

    }
}
