package com.reservation.onlineTicket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@EnableAutoConfiguration
@SpringBootApplication
public class OnlineTicketApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTicketApplication.class, args);
	}

}
