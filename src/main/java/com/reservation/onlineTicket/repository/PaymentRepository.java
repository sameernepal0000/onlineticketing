package com.reservation.onlineTicket.repository;

import com.reservation.onlineTicket.entity.Payment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment,Integer> {
}
