package com.reservation.onlineTicket.repository;
import com.reservation.onlineTicket.entity.ReservationTicketDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface Reservationrepository extends JpaRepository<ReservationTicketDetails,Integer> {

}
