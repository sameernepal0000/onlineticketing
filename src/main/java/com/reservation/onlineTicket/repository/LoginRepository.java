package com.reservation.onlineTicket.repository;

import com.reservation.onlineTicket.entity.LoginCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<LoginCredentials,Long> {

    LoginCredentials findByUsername(String userName);
}
