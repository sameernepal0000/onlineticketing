package com.reservation.onlineTicket;

import com.reservation.onlineTicket.clientdao.Clientdao;
import com.reservation.onlineTicket.entity.Payment;
import com.reservation.onlineTicket.entity.ReservationTicketDetails;
import com.reservation.onlineTicket.models.LoginDetails;
import com.reservation.onlineTicket.models.ReservationDetails;
import com.reservation.onlineTicket.models.Response;
import com.reservation.onlineTicket.repository.PaymentRepository;
import com.reservation.onlineTicket.repository.Reservationrepository;
import com.reservation.onlineTicket.util.RandomNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class IndexController {
    @Autowired
    Clientdao clientdao;
    @Autowired
    Reservationrepository reservationrepository;
    @Autowired
    PaymentRepository paymentRepository;

    @GetMapping()
    public String getLoginPage() {
        return "login";
    }

    @PostMapping("/login")
    public String LoginSubmit(LoginDetails loginDetails,Model model) {

        Response response=clientdao.checkLogindetail(loginDetails);
        if(response.isStatus()) {
            model.addAttribute("massage",response.getMassage());
            return "index";
        }else{
            model.addAttribute("massage",response.getMassage());
            return "login";
        }

    }

    @GetMapping("/saveReservationDetails")
    public String  reservationDetails(ReservationDetails details,HttpSession session,Model model){
        Response response=new Response();
        try{
            String Uniqueno= RandomNumber.getRandomNumericNumber(7);
            details.setUniqueNumber(Uniqueno);
            session.setAttribute("moviesname",details.getMoviename());
            session.setAttribute("fullname",details.getFullname());
            session.setAttribute("childrens",details.getChildrens());
            session.setAttribute("circle",details.getCircle());
            session.setAttribute("tickets",details.getTickets());
            String totalAmountTopay=Integer.toString((Integer.parseInt(details.getChildrens())*100)+((Integer.parseInt(details.getTickets())*200)));
            session.setAttribute("totalamount",totalAmountTopay);
            session.setAttribute("showdate",details.getShowdate());
            session.setAttribute("uniqueno",Uniqueno);
            response=clientdao.saveReservationDetails(details);

            model.addAttribute("moviesname",session.getAttribute("moviesname"));
            model.addAttribute("fullname",session.getAttribute("fullname"));
            model.addAttribute("childrens",session.getAttribute("childrens"));
            model.addAttribute("circle",session.getAttribute("circle"));
            model.addAttribute("tickets",session.getAttribute("tickets"));
            model.addAttribute("totalamount",session.getAttribute("totalamount"));
            model.addAttribute("showdate",session.getAttribute("showdate"));


        }catch (Exception e){
            e.printStackTrace();
        }

       return "payment";
    }

    @PostMapping("/savePaymentDetails")
    public String savePaymentDetails(ReservationDetails details,HttpSession session,Model model){
        Response response=new Response();
        try
        {
        String unique=session.getAttribute("uniqueno").toString();
         details.setUniqueNumber(unique);
            System.out.println(details.getCardNo());
            response=clientdao.savePayment(details);


           model.addAttribute("massage",response.getMassage());
           model.addAttribute("UniqueNumberForCustomer","MOVIES@"+unique);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "index";
    }


    @GetMapping("/reshistory")
    public String ReservationHistory(Model model){

        List<ReservationTicketDetails> details=reservationrepository.findAll();

        model.addAttribute("details",details);

        return "reservationHistory";
    }


    @GetMapping("/paymentHistory")
    public String paymentHistory(Model model){

        List<Payment> detailss=paymentRepository.findAll();

        model.addAttribute("payment",detailss);

        return "paymenthistory";
    }


}
