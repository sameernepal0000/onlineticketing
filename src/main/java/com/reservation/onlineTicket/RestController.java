package com.reservation.onlineTicket;
import com.reservation.onlineTicket.dao.OnlineTicketDao;
import com.reservation.onlineTicket.models.AuthenticationRequest;
import com.reservation.onlineTicket.models.AuthenticationResponse;
import com.reservation.onlineTicket.models.ReservationDetails;
import com.reservation.onlineTicket.models.Response;
import com.reservation.onlineTicket.services.MyUserDetailsService;
import com.reservation.onlineTicket.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;


@Controller
public class RestController {

    @Autowired
    private AuthenticationManager authenticationManager;

     @Autowired
     MyUserDetailsService userDetailsService;

     @Autowired
     JwtUtil jwtUtil;

    @Autowired
    OnlineTicketDao onlineTicketDao;


    @RequestMapping (value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        }
        catch (BadCredentialsException e){
            throw  new Exception("Incorrect Username and password");
        }
        final UserDetails userDetails= userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String jwt=jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }


    @RequestMapping (value = "/saveReservationdetaion", method = RequestMethod.POST)
    public Response saveReservationdetaion(@RequestBody ReservationDetails details) throws Exception {
        Response response1=new Response();
        try {

           response1=onlineTicketDao.SaveReservationDetails(details);
            System.out.println(response1);

        }
        catch (Exception e){
           e.printStackTrace();
        }
       return response1;
    }

    @RequestMapping (value = "/savePayment", method = RequestMethod.POST)
    public Response savePayment(@RequestBody ReservationDetails details) throws Exception {
        Response response1=new Response();
        try {

            response1=onlineTicketDao.savePayment(details);
            System.out.println(response1);

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return response1;
    }


    @RequestMapping (value = "/getReservationHistory", method = RequestMethod.POST)
    public List<ReservationDetails> getReservationHistory() throws Exception {
        List<ReservationDetails> reservationhistory=new ArrayList<>();
        try {

            reservationhistory=onlineTicketDao.getReservationHistory();
            System.out.println(reservationhistory);

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return reservationhistory;
    }

}
