package com.reservation.onlineTicket.dao;


import com.reservation.onlineTicket.models.LoginDetails;
import com.reservation.onlineTicket.models.ReservationDetails;
import com.reservation.onlineTicket.models.Response;

import java.util.List;

public interface OnlineTicketDao {
    boolean startDataBase();

    Response SaveReservationDetails(ReservationDetails details);

    Response savePayment(ReservationDetails details);

    List<ReservationDetails> getReservationHistory();


//    LoginDetails getUser(String userName);
}
