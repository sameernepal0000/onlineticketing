package com.reservation.onlineTicket.dao;

import com.reservation.onlineTicket.entity.LoginCredentials;
import com.reservation.onlineTicket.entity.Payment;
import com.reservation.onlineTicket.entity.ReservationTicketDetails;
import com.reservation.onlineTicket.models.LoginDetails;
import com.reservation.onlineTicket.models.ReservationDetails;
import com.reservation.onlineTicket.models.Response;
import com.reservation.onlineTicket.util.DateTimeUtil;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class OnlineTicketDaoImp implements OnlineTicketDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean startDataBase() {
        LoginCredentials details = new LoginCredentials();
        try {
            Query query = em.createQuery("Select user from LoginCredentials user");
            List<LoginCredentials> login = query.getResultList();

            if (login.isEmpty()) {
                details.setUserName("admin");
                details.setPasswords("admin@123");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date logindata = new Date();
                details.setCreatedDate(DateTimeUtil.getCotivitiTimeStamp(format.format(logindata)));
                em.persist(details);
            }

            return true;

        } catch (Exception e1) {

            return false;
        }
    }

    @Override
    public Response SaveReservationDetails(ReservationDetails details) {
        Response response1=new Response();
        try{
            ReservationTicketDetails reservationTicketDetails=new ReservationTicketDetails();
            reservationTicketDetails.setBookeddate(new Date());
            reservationTicketDetails.setChildrens(details.getChildrens());
            reservationTicketDetails.setCircle(details.getCircle());
            reservationTicketDetails.setFullname(details.getFullname());
            reservationTicketDetails.setMoviename(details.getMoviename());
            reservationTicketDetails.setTotalamount(Integer.toString((Integer.parseInt(details.getChildrens())*100)+((Integer.parseInt(details.getTickets())*200))));
            reservationTicketDetails.setPhone(details.getPhone());
            reservationTicketDetails.setShowdate(details.getShowdate());
            reservationTicketDetails.setTickets(details.getTickets());
            reservationTicketDetails.setUniqueNumber(details.getUniqueNumber());
            reservationTicketDetails.setPaymentStatus("UN-PAID");
            em.persist(reservationTicketDetails);
            response1.setMassage("Massage:Sucessfully Added");
            response1.setStatus(true);

        }catch (Exception e){
            e.printStackTrace();
            response1.setMassage("Massage:Unable to  Added");
            response1.setStatus(false);
        }
        return response1;
    }



    @Override
    public Response savePayment(ReservationDetails details) {
        Response response1=new Response();
        try{
            Payment payment=new Payment();
            payment.setCardNo(details.getCardNo());
            payment.setCvcNo(details.getCvcNo());
            payment.setMonths(details.getMonths());
            payment.setFullname(details.getFullname());
            payment.setYear(details.getYear());
            payment.setUniqueNumber(details.getUniqueNumber());
            payment.setPaiddate(new Date());
            em.persist(payment);

            ReservationTicketDetails reservationTicketDetails=new ReservationTicketDetails();
            Query query=em.createQuery("Select data from ReservationTicketDetails data where data.uniqueNumber=?1");
            query.setParameter(1,details.getUniqueNumber());
            reservationTicketDetails=(ReservationTicketDetails)query.getSingleResult();
            reservationTicketDetails.setPaymentStatus("PAID");
            em.merge(reservationTicketDetails);
            response1.setMassage("Massage:Sucessfully Added");
            response1.setStatus(true);

        }catch (Exception e){
            e.printStackTrace();
            response1.setMassage("Massage:Unable to  Added");
            response1.setStatus(false);
        }
        return response1;
    }


    @Override
    public List<ReservationDetails> getReservationHistory() {
        List<ReservationDetails> reservationDetails=new ArrayList<>();
        try
        {
           List<ReservationTicketDetails> reservationTicketDetails=new ArrayList<>();
           Query query=em.createQuery("select data from ReservationTicketDetails data where data.paymentStatus=?1");
           query.setParameter(1,"PAID");
           reservationTicketDetails=query.getResultList();

          System.out.println(reservationTicketDetails);
          for(ReservationTicketDetails details:reservationTicketDetails) {
              ReservationDetails reservationDetails1=new ReservationDetails();
              reservationDetails1.setFullname(details.getFullname());
              reservationDetails1.setChildrens(details.getChildrens());
              reservationDetails1.setMoviename(details.getMoviename());
              reservationDetails1.setCircle(details.getCircle());
              reservationDetails1.setTickets(details.getTickets());
              reservationDetails1.setTotalamount(details.getTotalamount());
              reservationDetails1.setShowdate(details.getShowdate());

              Payment payment=new Payment();
              Query query1=em.createQuery("Select data from Payment data where data.uniqueNumber=?1");
              query1.setParameter(1,details.getUniqueNumber());
              payment=(Payment)query1.getSingleResult();

              reservationDetails1.setCardNo(payment.getCardNo());
              reservationDetails1.setYear(payment.getYear());
              reservationDetails1.setMonths(payment.getMonths());

              reservationDetails.add(reservationDetails1);
              System.out.println(reservationDetails1);

            }


        }catch (Exception e){
            e.printStackTrace();
        }
        return reservationDetails;
    }
}



  /*  @Override
    public LoginDetails getUser(String userName) {

        LoginDetails details = new LoginDetails();
        try {
            Query query = em.createQuery("Select user from LoginCredentials user where user.userName=?1");
            query.setParameter(1, userName);
            LoginCredentials login = (LoginCredentials) query.getResultList();
            details.setUserName(login.getUserName());
            details.setPasswords(login.getPasswords());

        } catch (Exception e1) {
            e1.printStackTrace();


        }
        return details;
    }*/


